<?php

require_once 'SkyrockAPI.php';
require_once 'Helpers.php';

header( 'Content-Type: text/html; charset=utf-8' );
header( 'X-Robots-Tag: noindex' );
header( 'Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN'] );
header( 'Access-Control-Allow-Credentials: true' );

define('DEBUG_MODE', false);

if (isset($_REQUEST['action'])) {
    if ($_REQUEST['action'] == 'xhr_get_user') {
        $SkyrockAPI = new SauveTonSkyblog\SkyrockAPI($_REQUEST['username']);
        echo json_encode($SkyrockAPI->get_user());
    } elseif ($_REQUEST['action'] == 'xhr_get_friends') {
        $SkyrockAPI = new SauveTonSkyblog\SkyrockAPI($_REQUEST['username']);
        echo json_encode($SkyrockAPI->get_friends());
    } elseif ($_REQUEST['action'] == 'xhr_get_archive') {
        $SkyrockAPI = new SauveTonSkyblog\SkyrockAPI($_REQUEST['username']);
        echo json_encode($SkyrockAPI->get_archive());
    }
}

return 0;
