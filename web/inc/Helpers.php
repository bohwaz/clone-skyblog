<?php

namespace SauveTonSkyblog;

use finfo;

class Helpers {

    /**
     * Debug logger
     */
    static public function debug(string $msg, ...$params): void
    {
        if (!defined('DEBUG_MODE') || !constant('DEBUG_MODE')) {
            return;
        }

        $time = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        $time = round($time * 1000);
        file_put_contents(__DIR__ . '/../debug.log', sprintf('[%s ms] ', $time) . vsprintf($msg, $params) . "\n", FILE_APPEND);
    }


    /**
     * Get full path for file from webpack
     *
     * @param $css
     * @return mixed
     */
    static function get_path_assets($css)
    {
        $map = $_SERVER['DOCUMENT_ROOT'] . '/build/manifest.json';
        static $hash = null;

        if ( null === $hash ) {
            $hash = file_exists( $map ) ? json_decode( file_get_contents( $map ), true ) : [];
        }

        if ( array_key_exists( $css, $hash ) ) {
            return $hash[ $css ];
        }

        return $css;
    }

    /**
     * Remove resizer in url
     *
     * @param $url
     * @return array|mixed|string|string[]
     */
    public function remove_width_resizer_params($url)
    {
        $pattern = '/(&amp;w=\d*)/i';
        preg_match_all($pattern, $url, $matches);

        if (isset($matches[1])) {
            foreach ($matches[1] as $widthParam) {
                $url = str_replace($widthParam, '', $url);
            }
        }

        return $url;
    }

    /**
     * Re-upload external images
     *
     * @param $html
     * @param $folderName
     * @return array|mixed|string|string[]
     */
    public function replace_external_images($html, $folderName)
    {
        $pattern = '/<img[^>]+src=["\']([^"\']+)["\'][^>]*>/i';
        preg_match_all($pattern, $html, $matches);

        if (!isset($matches[1])) {
            return $html;
        }

        $images = [];
        $path = __DIR__ . '/../tmp/'.$folderName.'/medias';

        foreach ($matches[1] as $image) {
            $images[$image] = $this->remove_width_resizer_params($image);
        }

        foreach (self::downloadUrlsTo($images, $path) as $url => $url_path) {
            $url_path = str_replace($path, 'medias/', $url_path);
            $images[array_search($url, $images)] = $url_path;
        }

        foreach ($images as $original_url => $url_path) {
            $html = str_replace($original_url, $url_path, $html);
        }

        return $html;
    }

    /**
     * Replace external url
     *
     * @param $url
     * @param $folderName
     * @return string
     */
    public function replace_external_images_url($url, $folderName)
    {
        $path = __DIR__ . '/../tmp/'.$folderName.'/medias';

        foreach (self::downloadUrlsTo([$url], $path) as $url => $new_path) {
            continue;
        }

        return 'medias/'.basename($new_path);
    }

    /**
     * Generate random name
     *
     * @return string
     */
    private function generate_random_name()
    {
        $str = rand();
        $result = md5($str);
        return $result;
    }

    /**
     * Download multiple URLs to a destination folder, return associative array URL => new_file_path
     */
    static public function downloadUrlsTo(array $urls, string $destination): array
    {
        $mh = curl_multi_init();
        $ch = [];
        $paths = [];

        foreach ($urls as $url) {
            $url_path = $destination . '/' . md5(random_bytes(10)) . '.' . self::get_url_extension($url);
            $paths[$url] = $url_path;

            if (file_exists($url_path)) {
                self::debug("NOT downloading %s: already exists: %s", $url, $url_path);
                continue;
            }

            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_HEADER, 0);

            $ch[$url] = $c;
            curl_multi_add_handle($mh, $c);
        }

        $active = null;
        //execute the handles
        do {
            $mrc = curl_multi_exec($mh, $active);
        }
        while ($mrc == \CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) != -1) {
                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }
        }

        // iterate through the handles and get your content
        foreach ($ch as $url => $c) {
            self::debug("Saving %s\n-> %s", $url, $paths[$url]);
            file_put_contents($paths[$url], curl_multi_getcontent($c));
            curl_multi_remove_handle($mh, $c);
        }

        curl_multi_close($mh);

        return $paths;
    }

    static protected function get_url_extension(string $url): string
    {
        if (preg_match('!\.(jpe?g|png|gif|webp)!i', $url, $match)) {
            return strtolower($match[1]);
        }

        return 'img';
    }
}