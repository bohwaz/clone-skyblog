<?php
namespace SauveTonSkyblog;

use DateTime;
use DateTimeZone;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SauveTonSkyblog\Helpers;
use SimpleXMLElement;
use ZipArchive;

/**
 * Class SkyrockAPI
 *
 */

class SkyrockAPI
{
    /**
     * @var string
     */
    private $baseURL;
    /**
     * @var mixed
     */
    private $username;
    /**
     * @var null
     */
    private $folderName;
    /**
     * @var null
     */
    private $htmlSidebarLeft;
    /**
     * @var Helpers
     */
    private $Helpers;
    /**
     * @var array
     */
    private $error_rate_limit;
    /**
     * @var null
     */
    private $htmlSidebarRight;
    /**
     * @var array
     */
    private $friends;

    function __construct($username)
    {
        $this->baseURL = 'https://api.skyrock.mobi/v2/';
        $this->username = $username;
        $this->folderName = null;
        $this->htmlSidebarLeft = null;
        $this->htmlSidebarRight = null;
        $this->friends = [];
        $this->Helpers = new Helpers();
        $this->get_user();
        $this->error_rate_limit = [
            'error'     => true,
            'message'   => "Étant limité par l'API de Skyrock (...), vous pourrez de nouveau exporter votre skyblog dans une heure."
        ];
    }

    /**
     * Get rate limit
     *
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function get_rate_limit()
    {
        return $this->getDataFromUrl($this->get_endpoint('rateLimit'));
    }

    /**
     * Get user
     *
     * @return array|SimpleXMLElement
     * @throws Exception
     */
    public function get_user()
    {
        if (!file_exists(__DIR__ . '/../cache')) {
            mkdir(__DIR__ . '/../cache', 0700, true);
        }

        $path = __DIR__ . '/../cache/user_' . md5($this->username) . '.json';

        if (file_exists($path)) {
            Helpers::debug('Fetching user info from cache: %s', $this->username);
            return json_decode(file_get_contents($path));
        }
        else {
            $data = $this->getDataFromUrl($this->get_endpoint('user'));
            file_put_contents($path, json_encode($data));
            return $data;
        }
    }

    /**
     * Get blog data
     *
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function get_blog_data()
    {
        return $this->getDataFromUrl($this->get_endpoint('blogData'));
    }

    /**
     * Get relationship
     *
     * @return array|SimpleXMLElement
     * @throws Exception
     */
    public function get_friends()
    {
        return $this->getDataFromUrl($this->get_endpoint('friends'));
    }

    /**
     * Get medias by post id
     *
     * @param $id_post
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function get_media_post($id_post)
    {
        return $this->getDataFromUrl($this->get_endpoint('mediaPost', null, $id_post));
    }

    /**
     * Get comments by post id
     *
     * @param $id_post
     * @param $page
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function get_comments_post($id_post, $page = null)
    {
        return $this->getDataFromUrl($this->get_endpoint('commentsPost', $page, $id_post));
    }

    /**
     * Genrate archive of blog
     *
     * @return null[]
     * @throws Exception
     */
    public function get_archive(): array
    {
        // Get data for blog
        $data_blog = [
            'title' => '',
            'avatar' => '',
            'description' => '',
            'id_skin' => 1,
            'created_at' => '',
            'updated_at' => '',
            'nb_posts' => '',
            'nb_comments' => '',
            'nb_friends' => '',
            'nb_visits' => ''
        ];

        $blog = $this->get_blog_data();

        if (isset($blog->title)) {
            $data_blog['title'] = $blog->title;
        }
        if (isset($blog->avatar_url)) {
            $data_blog['avatar_url'] = $blog->avatar_url;
        }
        if (isset($blog->description)) {
            $data_blog['description'] = $blog->description;
        }
        if (isset($blog->id_skin)) {
            $data_blog['id_skin'] = $blog->id_skin;
        }
        if (isset($blog->created_at)) {
            $data_blog['created_at'] = $blog->created_at;
        }
        if (isset($blog->updated_at)) {
            $data_blog['updated_at'] = $blog->updated_at;
        }
        if (isset($blog->nb_posts)) {
            $data_blog['nb_posts'] = $blog->nb_posts;
        }
        if (isset($blog->nb_comments)) {
            $data_blog['nb_comments'] = $blog->nb_comments;
        }
        if (isset($blog->nb_friends)) {
            $data_blog['nb_friends'] = $blog->nb_friends;
        }
        if (isset($blog->nb_visits)) {
            $data_blog['nb_visits'] = $blog->nb_visits;
        }

        // Create new folder and copy files
        $this->create_new_base_for_user();

        Helpers::debug('Generating sidebars');

        $this->htmlSidebarLeft = $this->generate_html_sidebar_left($data_blog);
        $this->htmlSidebarRight = $this->generate_html_sidebar_right($data_blog);

        Helpers::debug('Getting posts');

        // Get data for posts
        $current_page = 1;
        $data = $this->getDataFromUrl($this->get_endpoint('blogPosts', $current_page));
        $posts = '';
        if (! empty($data)) {
            $nb_page = $data->max_page;

            while($current_page <= $nb_page) {

                if ($current_page != 1) {
                    $data = $this->getDataFromUrl($this->get_endpoint('blogPosts', $current_page));
                }

                foreach ((array) $data->posts as $post) {

                    $medias = $this->get_media_post($post->id_post);
                    $comments = $this->get_comments_post($post->id_post);

                    if (isset($comments->max_page) && $comments->max_page > 0) {
                        $this->generate_post_comments($comments, $post->id_post);
                    }

                    // Detect first photo in content
                    if (isset($medias[0])) {
                        $matches = $this->get_src_img_attribut($medias[0]->media_html);
                        if (isset($matches[1][0]) && str_contains($post->text, $matches[1][0])) {
                            $medias = [];
                        } else {
                            $medias[0]->media_html = $this->Helpers->replace_external_images($medias[0]->media_html, $this->folderName);
                        }
                    }

                    // Reupload all medias
                    $post->text = $this->Helpers->replace_external_images($post->text, $this->folderName);

                    $data_post = [
                        'id'            => $post->id_post,
                        'title'         => $post->title,
                        'content'       => $post->text,
                        'created_at'    => (new DateTime('@' . $post->created_at))->setTimezone(new DateTimeZone('Europe/Paris')),
                        'updated_at'    => (new DateTime('@' . $post->updated_at))->setTimezone(new DateTimeZone('Europe/Paris')),
                        'media_align'   => $post->media_align,
                        'nb_comments'   => $post->nb_comments,
                        'medias'        => $medias
                    ];

                    $posts .= $this->generate_html_post($data_post);
                }

                $current_page++;
            }
        }

        // Write in file
        $this->write_html_index($posts, 33, 'index.html');
        $this->write_html_index($this->htmlSidebarRight, 28, 'index.html');
        $this->write_html_index($this->htmlSidebarLeft, 22, 'index.html');

        // Create zip
        $this->create_zipfile_folder($data_blog['id_skin']);

        return [
            'folderName' => $this->folderName
        ];
    }

    /**
     * Create folder and copy base files
     */
    private function create_new_base_for_user(): void
    {
        $this->folderName = $this->username;

        // For debug, re-use the same directory, so that we don't re-download everything every time
        if (!defined('DEBUG_MODE') || !constant('DEBUG_MODE')) {
            $this->folderName .= '-' . $this->generate_random_name();
        }

        $path = __DIR__ . "/../tmp/" . $this->folderName;

        mkdir($path, 0700, true);
        mkdir($path . '/commentaires', 0700);
        mkdir($path . '/medias', 0700);
        copy(__DIR__ . "/../base/index.html", $path . "/index.html" );
    }

    /**
     * Create ZIP file from folder
     *
     * @param $id_skin
     */
    private function create_zipfile_folder($id_skin): void
    {
        $zip = new ZipArchive();

        $path = __DIR__ . "/../archives";

        if (!file_exists($path)) {
            mkdir($path, 0700, true);
        }

        $pathFile = $path . "/" . $this->folderName . ".zip";

        Helpers::debug('Writing archive to %s', $pathFile);

        if (file_exists($pathFile)) {
            unlink($pathFile);
        }

        if (!$zip->open($pathFile, ZipArchive::CREATE)) {
            die ("Could not open archive");
        }
        $zip->addEmptyDir("css");
        $zip->addEmptyDir("images");
        $zip->addEmptyDir("medias");
        $zip->addEmptyDir("commentaires");
        $zip->addFile(__DIR__ . "/../base/images/commentview.png", "images/commentview.png");
        $zip->addFile(__DIR__ . "/../base/css/common.css", "css/common.css");
        $zip->addFile(__DIR__ . "/../base/css/tpl.css", "css/tpl.css");
        $zip->addFile(__DIR__ . "/../base/css/themes/" . $id_skin . ".css", "css/theme.css");
        $zip->addFile(__DIR__ . "/../tmp/" . $this->folderName . "/index.html", "index.html");

        // Add all comments files
        $files_comments = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . "/../tmp/" . $this->folderName . "/commentaires"), RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($files_comments as $name => $file) {
            if (! $file->isDir()) {
                // Get real and relative path for current file
                $fileName = $file->getFilename();
                $filePath = $file->getRealPath();

                // Add current file to archive
                $zip->addFile($filePath, 'commentaires/' . $fileName);
            }
        }

        // Add all medias
        $files_medias = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . "/../tmp/" . $this->folderName . "/medias"), RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($files_medias as $name => $file) {
            if (! $file->isDir()) {
                // Get real and relative path for current file
                $fileName = $file->getFilename();
                $filePath = $file->getRealPath();

                // Add current file to archive
                $zip->addFile($filePath, 'medias/' . $fileName);
            }
        }

        $zip->close();

        Helpers::debug('Archive written');

        // Remove tmp folder, unless we are in debug mode, we want to keep the data to avoid re-download
        if (!defined('DEBUG_MODE') || !constant('DEBUG_MODE')) {
            $this->remove_directory_recursive(__DIR__ . "/../tmp/" . $this->folderName);
            Helpers::debug('Directory cleaned');
        }
    }

    /**
     * Remove directory not empty
     *
     * @param $dir
     */
    private function remove_directory_recursive($dir)
    {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) $this->remove_directory_recursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }

    /**
     * Write in file
     *
     * @param $html
     * @param $row
     * @param $file
     */
    private function write_html_index($html, $row, $file)
    {
        $filename = __DIR__ . "/../tmp/" . $this->folderName. "/" . $file;
        $lines = file($filename);
        $lines[$row] = $html;
        file_put_contents($filename , $lines);
    }

    /**
     * Generate random name
     *
     * @return string
     */
    private function generate_random_name()
    {
        $str = rand();
        $result = md5($str);
        return $result;
    }

    /**
     * Generate HTML for post
     *
     * @param $data_post
     * @return string
     */
    private function generate_html_post($data_post)
    {
        $thumbnail = '';
        if (! empty($data_post['medias'])) {
            $thumbnail = '
                <div class="image-container '.$this->get_aligment_media($data_post['media_align']).'">
                    '.(isset($data_post['medias'][0]) ? $data_post['medias'][0]->media_html : '').'
                </div>
            ';
        }

        return '
            <div id="'.$data_post['id'].'" class="bloc article_item '.(empty($data_post['medias']) ? 'isText' : 'hasimage').'">
                <div class="frontadmin-blog-article-wrapper">
                    <h2 class="bloc_title">
                        <a class="plink" href="#" itemprop="name">'.$data_post['title'].'</a>
                    </h2>
                </div>
                <div class="post clearfix">
                    '.$thumbnail.'
                    <div class="text-image-container">
                        '.$data_post['content'].'
                    </div>
                </div>
                <div class="article_bottom">
                    <div class="commentaires clearfix">
                        <div class="floatleft">
                            <a class="commentview" rel="nofollow" href="'.($data_post['nb_comments'] > 0 ? 'commentaires/commentaires-'.$data_post['id'].'.html' : '#').'">
                                '.$data_post['nb_comments'].'
                            </a>
                        </div>
                    </div>
                    <div class="date">
                        <p class="created_on">
                            <span>
                                <time itemprop="dateCreated">
                                    Posté le '.$data_post['created_at']->format('l d F Y H:i') .'
                                </time>
                            </span>
                        </p>
                        <div class="floatleft" style="position:relative; bottom: 5px; left: 3px;"></div>
                            <p class="modified_on">
                                <time itemprop="dateModified">
                                    Modifié le '.$data_post['updated_at']->format('l d F Y H:i').'
                                </time>
                            </p>
                        </div>
                    </div>
                </div>
        ';
    }

    /**
     * Generate HTML for post
     *
     * @param $data_post
     * @return string
     */
    private function generate_html_comment($data_comment)
    {
        return '
            <div class="comment-item identified">
                <div class="clearfix">
                    <p class="comment_info">
                        <strong>' . $data_comment['username'] . '</strong>,
                        <strong>Posté le </strong>' . $data_comment['date']->format('l d F Y H:i') . '
                    </p>
                    <p>' . $data_comment['content'] . '</p>
                </div>
            </div>
            <hr>
        ';
    }

    /**
     * Get aligment for media
     *
     * @param $align_media
     * @return string
     */
    private function get_aligment_media($align_media)
    {
        switch ($align_media) {
            case 'center_before_content' || 'center_after_content':
                $align = 'center';
                break;
            case 'float_right':
                $align = 'right';
                break;
            case 'float_left':
                $align = 'left';
                break;
        }

        return $align;
    }

    /**
     * Generate HTML for sidebar (left)
     *
     * @param $data_blog
     * @return string
     */
    private function generate_html_sidebar_left($data_blog)
    {
        $avatar = '';
        if ($data_blog['avatar_url'] !== false) {
            $avatar = '<img class="avatar" src="'.$this->Helpers->replace_external_images_url($data_blog['avatar_url'], $this->folderName).'" />';
        }

        return '
            <h1 class="blogtitle" itemprop="name">
                '.$data_blog['title'].'
            </h1>

            <div class="mini-profil">
                <div class="mini-profil-avatar">
                    '.$avatar.'
                    <p class="infos">
                        <a class="" href="#">
                            '.$this->username.'
                        </a>
                    </p>
                </div>
            </div>
            <h4>Description :</h4>
            <p class="description" itemprop="description">
                '.$data_blog['description'].'
            </p>
        ';
    }

    /**
     * Generate HTML for sidebar (right)
     *
     * @param $data_blog
     * @return string
     */
    private function generate_html_sidebar_right($data_blog)
    {
        $created_date = (new DateTime('@' . $data_blog['created_at']))->setTimezone(new DateTimeZone('Europe/Paris'));
        $updated_date = (new DateTime('@' . $data_blog['updated_at']))->setTimezone(new DateTimeZone('Europe/Paris'));

        $friends = '';
        $this->friends = $this->get_friends();
        if (! empty($this->friends->relations)) {
            foreach ($this->friends->relations as $friend) {
                $friends .= '
                    <li class="mini-profil-50 mn-card-wrapper first">
                        <a class="avatar-link is_a_friend" href="#">
                            <img class="avatar" itemprop="colleagues" title="' . $friend->username . '" alt="' . $friend->username . '" src="' . $this->Helpers->replace_external_images_url($friend->avatar_url, $this->folderName) . '" >
                        </a>
                    </li>
                ';
            }
        }

        return '
            <div id="bloginfo" class="sidebar-info-bloc">
                <h3><span>Infos</span></h3>
                <ul>
                    <li>Création : <em>'.$created_date->format('d/m/Y à H:i').'</em></li>
                    <li>Mise à jour : <em>'.$updated_date->format('d/m/Y à H:i').'</em></li>
                    <li><em>'.$data_blog['nb_posts'].'</em> articles</li>
                    <li><em>'.$data_blog['nb_comments'].'</em> commentaires</li>
                    <li><em>'.$data_blog['nb_friends'].'</em> amis</li>
                    <li><em>'.$data_blog['nb_visits'].'</em> visites</li>
                </ul>
            </div>
            <div id="fans" class="sidebar-info-bloc thumbnails">
                <h3>
                    <a href="">
                        <span>Ses amis</span>
                        <span class="nb">('.$data_blog['nb_friends'].')</span>
                        <span class="pointe_border"></span>
                    </a>
                </h3>
                <ul class="liste-amis clearfix">
                    '.$friends.'
                </ul>
            </div>
        ';
    }

    /**
     * Generate post comments
     *
     * @param $data_comments
     * @param $id_post
     * @return void
     * @throws Exception
     */
    private function generate_post_comments($data_comments, $id_post)
    {
        $current_page = 1;
        $nb_page = $data_comments->max_page;
        $comments = '';

        while($current_page <= $nb_page) {

            if ($current_page != 1) {
                $data_comments = $this->get_comments_post($id_post, $current_page);
            }

            foreach ((array) $data_comments->comments as $comment) {
                $data_comment = [
                    'username'  => ($comment->author->username ?? "Anonyme"),
                    'date'      => (new DateTime('@' . $comment->date))->setTimezone(new DateTimeZone('Europe/Paris')),
                    'content'   => $comment->content
                ];

                $comments .= $this->generate_html_comment($data_comment);
            }

            $current_page++;
        }

        $this->create_new_file_comment_post($id_post, $comments);
    }

    private function create_new_file_comment_post($id_post, $comments)
    {
        copy(__DIR__ . "/../base/commentaires/commentaire.html", __DIR__ . "/../tmp/" . $this->folderName . "/commentaires/commentaires-" . $id_post . ".html" );

        // Write in file
        $this->write_html_index($comments, 44, 'commentaires/commentaires-' . $id_post . '.html');
        $this->write_html_index('<a href="../index.html#'.$id_post.'">Retour au blog de ' . $this->username . '</a>', 33, 'commentaires/commentaires-' . $id_post . '.html');
        $this->write_html_index(str_replace('medias/', __DIR__ . '/../medias/', $this->htmlSidebarRight), 28, 'commentaires/commentaires-' . $id_post . '.html');
        $this->write_html_index(str_replace('medias/', __DIR__ . '/../medias/', $this->htmlSidebarLeft), 22, 'commentaires/commentaires-' . $id_post . '.html');
    }

    private function get_src_img_attribut($html): array
    {
        $pattern = '/src=["\']([^"\']+)["\'][^>]/m';
        preg_match_all($pattern, $html, $matches);

        return $matches;
    }

    /**
     * Get data from URL
     *
     * @param $url
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function getDataFromUrl($url)
    {
        Helpers::debug('Requesting %s', $url);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Accept-Language: fr',
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        $result = curl_exec($curl);

        Helpers::debug('Got result of %d bytes', strlen($result));

        return json_decode($result);
    }

    /**
     * Get endpoints
     *
     * @param $name
     * @param null $page
     * @param null $id_post
     * @return string
     */
    private function get_endpoint($name, $page = null, $id_post = null): string
    {
        switch ($name) {
            case 'rateLimit':
                $route = 'check_rate_limit.json';
                break;
            case 'user':
                $route = 'user/get.json?username='.$this->username;
                break;
            case 'friends':
                $route = 'relationship/list_relations.json?kind=friend&username='.$this->username;
                break;
            case 'blogPosts':
                $route = 'blog/list_posts.json?username='.$this->username.'&page='.$page;
                break;
            case 'mediaPost':
                $route = 'blog/list_post_medias.json?id_post='.$id_post.'&username='.$this->username;
                break;
            case 'blogData':
                $route = 'blog/get.json?username='.$this->username;
                break;
            case 'commentsPost':
                $route = 'blog/list_post_comments.json?id_post='.$id_post.'&username='.$this->username;
                if (! is_null($page)) {
                    $route .= '&page='.$page;
                }
                break;
        }

        return $this->baseURL . $route;
    }
}