 const Encore = require('@symfony/webpack-encore');
// const CleanWebpackPlugin = require('clean-webpack-plugin');

Encore
// directory where compiled assets will be stored
    .setOutputPath('build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    .setManifestKeyPrefix('build/')

    .cleanupOutputBeforeBuild()

    .configureFilenames({
        css: 'css/[name].[hash].css',
        js: 'js/[name].[hash].js'
    })

    .enableSassLoader()

    .addStyleEntry('main', './assets/scss/main.scss')
    .addEntry('app', './assets/js/app.js')

    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()

    // Images
    .copyFiles({
        from: 'assets/images',
        // optional target path, relative to the output dir
        to: 'images/[path][name].[ext]',
        // if versioning is enabled, add the file hash too
        //to: 'images/[path][name].[hash:8].[ext]',
        // only copy files matching this pattern
        //pattern: /\.(png|jpg|jpeg)$/
    })

// uncomment if you use TypeScript
//.enableTypeScriptLoader()

// uncomment if you use Sass/SCSS files


// uncomment if you're having problems with a jQuery plugin
//.autoProvidejQuery()
;

// const config = Encore.getWebpackConfig();
//
// for (const plugin of config.plugins) {
//     if ((plugin instanceof CleanWebpackPlugin) && plugin.options) {
//         plugin.options.watch = true;
//     }
// }

module.exports = Encore.getWebpackConfig();