<?php
    require_once 'inc/Helpers.php';
    $Helpers = new SauveTonSkyblog\Helpers();
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sauve ton skyblog</title>
    <meta name="description" content="Vous êtes nostalgique de votre passé virtuel et ne voulez pas laisser votre précieux Skyblog disparaître dans les méandres de l'histoire en ligne ?">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $Helpers->get_path_assets('build/images/favicon/favicon-32x32.png') ?>">
    <link rel="icon" href="<?php echo $Helpers->get_path_assets('build/images/favicon/favicon.ico') ?>"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $Helpers->get_path_assets('build/main.css') ?>" />
    <meta name="robots" content="index, follow" />
    <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <!-- Matomo -->
    <script>
        var _paq = window._paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u="//matomo.anthony-martin.fr/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
    </script>
    <!-- End Matomo Code -->

</head>
<body>
    <header class="header">
        <div class="header_container">
            <img src="build/images/logo.png" alt="Sauve ton skyblog">
        </div>
    </header>

    <section class="findSkyblog">
        <div class="findSkyblog_container">
            <div class="findSkyblog_top">
                <p>
                    Vous êtes nostalgique de votre passé virtuel et ne voulez pas laisser <mark>votre précieux Skyblog</mark> <br>
                    disparaître dans les méandres de l'histoire en ligne ? <br>
                </p>
                <p>
                    Ce site vous permet de sauvegarder <mark>une copie complète</mark> de votre Skyblog avant qu'il ne tire sa révérence le 21/08/2023.<br>
                    Préparez-vous à raviver vos souvenirs numériques et à préserver votre héritage en ligne
                </p>
            </div>
            <form class="findSkyblog_form">
                <div class="findSkyblog_form_field">
                    <input type="text" name="username" placeholder="lePtitB0G0Ss-4541" required>
                    <span>.skyrock.com</span>
                </div>
                <div class="findSkyblog_form_field">
                    <button class="checkAccount">Vérifier si mon blog existe</button>
                </div>
                <div class="findSkyblog_form_error"></div>
            </form>
            <div class="findSkyblog_profile --isHidden"></div>
        </div>
        <div class="findSkyblog_friends"></div>
    </section>

    <footer class="footer">
        <p>Ce site n'est pas affilié à skyrock.com</p>
        <p>Développé par <a href="https://twitter.com/Spidlace" target="_blank"><span>Spidlace</span></a></p>
        <script type="application/javascript" src="<?php echo $Helpers->get_path_assets('build/runtime.js') ?>"></script>
        <script type="application/javascript" src="<?php echo $Helpers->get_path_assets('build/app.js') ?>"></script>
        <script type="application/javascript">
            window.app.initModule('home');
        </script>
    </footer>
</body>
</html>