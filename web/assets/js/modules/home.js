class Module {

    // méthode constructor
    constructor() {
        this.moduleName = "home";
        this.username = null;
    }

    init() {

    }

    DOMReady() {
        this.onSubmitForm();
        this.onClickOnNoButton();
        this.onClickOnYesButton();
        this.onClickOnRestartButton();
    }

    onSubmitForm() {
        let $form = document.querySelector('.findSkyblog_form');
        $form.addEventListener('submit', (event) => {
            let $button = document.querySelector('.checkAccount');
            let $username = document.querySelector('input[name="username"]');

            if ($username.value === '') {
                event.preventDefault();
                return false;
            }

            // Disabled field & button
            $username.setAttribute('disabled', 'disabled');
            $button.setAttribute('disabled', 'disabled');

            // Add loader
            $button.classList.add('--isLoading');

            const data = new FormData();

            data.append('username', $username.value);
            data.append('action', 'xhr_get_user');

            fetch('/inc/ajax.php', {
                method: "POST",
                body: data,
                headers: {
                    'Accept': 'application/json',
                },
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.error !== undefined) {
                    alert(data.message);
                } else {
                    if (data.has_blog) {
                        $form.classList.add('--isHidden');
                        this.username = data.username;

                        let $profile = document.querySelector('.findSkyblog_profile');
                        $profile.innerHTML = '<p class="findSkyblog_profile_question">C\'est bien ta photo <mark>' + this.username + '</mark> ?</p>';
                        $profile.innerHTML += '<div class="findSkyblog_profile_frame"><img src="' + data.avatar_url + '" alt="Photo de profil de ' + this.username + '" width="100px" height="100px" /></div>';
                        $profile.innerHTML += '<div class="findSkyblog_profile_wrapperButtons"><button class="findSkyblog_profile_yes">Oui, j\'ai honte...</button><button class="findSkyblog_profile_no">Pas du tout !</button></div>';
                    } else {
                        let $error = document.querySelector('.findSkyblog_form_error');
                        $error.innerHTML = "<p>Désolé, il n'y a pas de blog associé à ce compte 😥</p>";
                    }
                }
            })
            .finally(() => {
                // Disabled field & button
                $username.removeAttribute('disabled');
                $button.removeAttribute('disabled');
                $username.value = '';

                // Add loader
                $button.classList.remove('--isLoading');
            })
            .catch((error) => {
                console.error(error);
                alert('Une erreur s\'est produite, merci de réessayer plus tard.');
                location.href = location.href;
            });

            event.preventDefault();
        })
    }
    onClickOnNoButton() {
        document.querySelector('body').addEventListener('click', (event) => {
            if (! event.target.closest('.findSkyblog_profile_no')) {
                return false;
            }

            let $form = document.querySelector('.findSkyblog_form');
            $form.classList.remove('--isHidden');

            let $profile = document.querySelector('.findSkyblog_profile');
            $profile.innerHTML = '';
        });
    }
    onClickOnYesButton() {
        document.querySelector('body').addEventListener('click', (event) => {
            let $button = event.target.closest('.findSkyblog_profile_yes');
            if (! $button) {
                return false;
            }

            // Remove "No" button
            let $buttonNo = document.querySelector('.findSkyblog_profile_no');
            $buttonNo.remove();

            // Enlarge your pe... button
            $button.classList.add('--isBig');
            $button.setAttribute('disabled', 'disabled');
            $button.innerHTML = 'Chargement';

            // Add warning
            let $profile = document.querySelector('.findSkyblog_profile');
            $profile.innerHTML += '<div class="findSkyblog_profile_warning"><p>Attention, le traitement peut prendre un certain temps, veuillez rester sur la page ! 😉</p></div>';

            this.xhrGetFriends(this.username)
            this.xhrDownZipBlog(this.username)
        });
    }
    onClickOnRestartButton() {
        document.querySelector('body').addEventListener('click', (event) => {
            let $button = event.target.closest('.findSkyblog_profile_restart');
            if (! $button) {
                return false;
            }

            // Clear profile & friends section
            let $profile = document.querySelector('.findSkyblog_profile');
            let $friends = document.querySelector('.findSkyblog_friends');
            $profile.innerHTML = '';
            $friends.innerHTML = '';

            // Show form
            let $form = document.querySelector('.findSkyblog_form');
            $form.classList.remove('--isHidden');
        });
    }
    xhrGetFriends(username) {
        const data = new FormData();

        data.append('username', username);
        data.append('action', 'xhr_get_friends');

        fetch('/inc/ajax.php', {
            method: "POST",
            body: data,
            headers: {
                'Accept': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.error !== undefined) {
                alert(data.message);
            } else {
                if (data.relations.length > 0) {
                    let list_friends = '';
                    let $fiends = document.querySelector('.findSkyblog_friends');
                    $fiends.innerHTML = '<div class="findSkyblog_container"><h2>En attendant, tu peux toujours visiter le skyblog de tes amis !</h2>';
                    $fiends.innerHTML += '<p>(et les inviter à télécharger le leur)</p></div>';
                    data.relations.forEach((friend) => {
                        list_friends += '<a href="https://' + friend.username + '.skyrock.com" title="Lien vers le skyblog de ' + friend.username + '" target="_blank" class="findSkyblog_friends_list_item"><img src="' + friend.avatar_url + '" alt="Photo de profil de ' + friend.username + '" width="100px" height="100px" />' + friend.username + '</a>'
                    })
                    $fiends.innerHTML += '<div class="findSkyblog_friends_list">' + list_friends + '</div>';
                }
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    xhrDownZipBlog(username) {
        const data = new FormData();

        data.append('username', username);
        data.append('action', 'xhr_get_archive');

        fetch('/inc/ajax.php', {
            method: "POST",
            body: data,
            headers: {
                'Accept': 'application/json',
            },
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.error !== undefined) {
                    alert(data.message);
                } else {
                    if (data.folderName) {
                        this.downloadFile(data.folderName);
                    }

                    // Add restart button
                    let $wrapperButtons = document.querySelector('.findSkyblog_profile_wrapperButtons');
                    $wrapperButtons.innerHTML += '<button class="findSkyblog_profile_restart" title="Recommencer"><svg width="800px" height="800px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path fill="none" stroke="#000000" stroke-width="2" d="M20,8 C18.5343681,5.03213345 15.4860999,3 11.9637942,3 C7.01333514,3 3,7.02954545 3,12 M4,16 C5.4656319,18.9678666 8.51390007,21 12.0362058,21 C16.9866649,21 21,16.9704545 21,12 M9,16 L3,16 L3,22 M21,2 L21,8 L15,8"/></svg></button>';

                    // Change button text
                    let $button = document.querySelector('.findSkyblog_profile_yes');
                    $button.classList.add('--isFinished')
                    $button.innerHTML = 'Et voilà ! Bienvenue dans le passé 😉';

                    // Remove warning
                    let $warning = document.querySelector('.findSkyblog_profile_warning');
                    $warning.remove();
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
    downloadFile(file) {
        // Create a link and set the URL using `createObjectURL`
        const link = document.createElement("a");
        link.style.display = "none";
        link.href = '/archives/' + file + '.zip';
        link.download = file;

        // It needs to be added to the DOM so it can be clicked
        document.body.appendChild(link);
        link.click();

        // To make this work on Firefox we need to wait
        // a little while before removing it.
        setTimeout(() => {
            URL.revokeObjectURL(link.href);
            link.parentNode.removeChild(link);
        }, 0);
    }
}

export default new Module();
