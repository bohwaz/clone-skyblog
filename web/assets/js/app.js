import homeModule from './modules/home';

class App {

    // Constructor method
    constructor() {
        this.moduleName = "app";
        this.modules = {};

        this.init();
    }

    init() {
        window.app = this;

        document.addEventListener('DOMContentLoaded', ()=>{
            this.DOMReady();
        });

        this.requireModules();
    };

    DOMReady() {
    }

    requireModules() {
        this.modules.home = homeModule;
    }

    initModule(moduleName, moduleRegistry = null) {

        if(!moduleRegistry){
            moduleRegistry = this.modules;
        }

        //console.debug('initModule : '+moduleName);
        if(!moduleRegistry[moduleName]){
            console.debug('no module :'+moduleName);
            return;
        }

        if (moduleRegistry[moduleName].init instanceof Function) {
            moduleRegistry[moduleName].init();
        } else {
            console.debug('no init() method for module named : '+moduleName);
        }

        if (moduleRegistry[moduleName].DOMReady instanceof Function) {
            if (document.readyState === 'loading') {  // Loading hasn't finished yet
                document.addEventListener('DOMContentLoaded', ()=>{
                    moduleRegistry[moduleName].DOMReady();
                });
            } else {  // `DOMContentLoaded` has already fired
                moduleRegistry[moduleName].DOMReady();
            }
        } else {
            console.debug('no DOMReady() method for module named : '+moduleName);
        }
    };
}

new App();